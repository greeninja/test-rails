module Api
    module V1
        class FormsController < ApplicationController
            skip_before_action :verify_authenticity_token
            respond_to :json

            def index
                respond_with Form.all
            end

            def show
                respond_with Form.find(params[:id])
            end

            def create
                @form = Form.new(form_params)
                if @form.save
                    render json: {:status => 'Success', :object => @form}
                else
                    render json: {:status => 'Error', :object => @form}
                end
            end

            def update
                @form = Form.find(params[:id])
                if @form.update(form_params)
                    render json: {:status => 'Success', :object => @form}
                else
                    render json: {:status => 'Error', :object => @form}
                end
            end

            def destroy
                respond_with Form.destroy(params[:id])
            end
        private
            def form_params
                params.require(:form).permit(:title, :firstname, :lastname, :knownas, :q1, :q2, :q3, :q4, :q5, :info)
            end
        end
    end
end
