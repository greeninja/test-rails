class CreateForms < ActiveRecord::Migration[6.0]
  def change
    create_table :forms do |t|
      t.string :title
      t.string :firstname
      t.string :lastname
      t.string :knownas
      t.string :q1
      t.string :q2
      t.string :q3
      t.string :q4
      t.string :q5
      t.text :info

      t.timestamps
    end
  end
end
