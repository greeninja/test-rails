#!/usr/bin/env ruby

require 'net/http'
require 'uri'
require 'json'
require 'yaml'
require_relative 'lib/names_array.rb'
require_relative 'lib/words_array.rb'

@uri = URI.parse("http://localhost:3000/api/v1/forms")
DEBUG = false

def build_array
  puts "Choosing random from #{@names_array.length} names"
  data = Hash.new
  fn = @names_array.sample
  titles = ['Miss', 'Mrs', 'Ms', 'Master', 'Mr', 'Dr', 'Mx' ]
  data['title'] = titles.sample
  data['firstname'] = fn
  data['lastname'] = @names_array.sample
  data['knownas'] = fn.split("").first(5).join("")
  data['q1'] = @words.sample
  data['q2'] = @words.sample
  data['q3'] = @words.sample
  data['q4'] = @words.sample
  data['q5'] = @words.sample
  data['info'] = `fortune`

  @data = data
  puts @data.to_yaml if DEBUG
end

def post_data
  header = {'Content-Type': 'application/json'}
  http = Net::HTTP.new(@uri.host, @uri.port)
  request = Net::HTTP::Post.new(@uri.request_uri, header)
  request.body = @data.to_json
  response = http.request(request)
  case response
  when Net::HTTPSuccess then
    puts "Successfully submitted"
  else
    puts "ERROR"
    puts response.value
  end
end

build_array
post_data
